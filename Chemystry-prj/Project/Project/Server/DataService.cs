﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Server
{
    class DataService
    {
        private readonly DataBase _dbContext;
        public DataService(DataBase dbContext)
        {
            _dbContext = dbContext;
        }

        public List<Users> GetAllUsers()
        {
            return _dbContext.Users.ToList();
        }


        public Users GetUser(string login, string password)
        {
            return _dbContext.Users.Where(el => el.Login.Equals(login) && el.Password.Equals(password)).FirstOrDefault();
        }

        public void InsertUser(Users user)
        {
            var userInDb = _dbContext.Users.FirstOrDefault(el => el.Login.Equals(user.Login));
            if (userInDb != null)
            {
                throw new ArgumentException("Логін існує в базі даних");
            }
            var query = @"INSERT INTO Users (Login, Password, UserName) VALUES (@Login, @Password, @UserName)";
            _dbContext.Database.ExecuteSqlCommand(query, new SqlParameter("@Login", user.Login), new SqlParameter("@Password", user.Password), new SqlParameter("@UserName", user.UserName));
            var userId = _dbContext.Users.FirstOrDefault(el => el.Login.Equals(user.Login)).UserId;
        }

        public List<MedicinesInfo> SearchByName(string name)
        {
            List<MedicinesInfo> medic = _dbContext.Database.SqlQuery<MedicinesInfo>("SELECT * FROM MedicinesInfo WHERE Name like '%" + name + "%'").ToList();
            return medic;
        }

        public List<MedicinesInfo> SearchByProd(string prod)
        {
            List<MedicinesInfo> medic = _dbContext.Database.SqlQuery<MedicinesInfo>("SELECT * from MedicinesInfo where Producer like '" + prod + "%'").ToList();
            return medic;
        }

        public List<MedicinesInfo> SearchByType(string type)
        {
            List<MedicinesInfo> medic = _dbContext.Database.SqlQuery<MedicinesInfo>("SELECT * from MedicinesInfo where Type like '" + type + "%'").ToList();
            return medic;
        }

        public List<MedicinesInfo> GetMedicines()
        {
            List<MedicinesInfo> medic = _dbContext.Database.SqlQuery<MedicinesInfo>("select * from MedicinesInfo").ToList();
            return medic;
        }

        public List<string> ShowProd()
        {
            List<string> prod = _dbContext.Database.SqlQuery<string>("select Producer from MedicinesInfo group by Producer").ToList();
            return prod;
        }

        public List<string> ShowType()
        {
            List<string> type = _dbContext.Database.SqlQuery<string>("select Type from MedicinesInfo group by Type").ToList();
            return type;
        }
    }
}
