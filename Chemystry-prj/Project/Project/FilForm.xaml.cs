﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Server;


namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for FilForm.xaml
    /// </summary>
    public partial class FilForm : Window
    {
        private SearchForm sForm;
        private readonly TcpService tcpService;
        List<string> prod, type;
        public FilForm(SearchForm sForm)
        {
            InitializeComponent();
            this.sForm = sForm;
            tcpService = new TcpService();
            Init();
        }

        private async void Init()
        {
            try
            {
                string request = tcpService.SerializeShowProd();
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                prod = tcpService.DeserializeShow(response);
                for (int i = 0; i < prod.Count; i++)
                {
                    comboMan.Items.Add(prod[i]);
                }

                string request2 = tcpService.SerializeShowType();
                byte[] data2 = await tcpService.CodeStreamAsync(request2);
                await SingletoneObj.Stream.WriteAsync(data2, 0, data2.Length);
                string response2 = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                type = tcpService.DeserializeShow(response2);
                for (int i = 0; i < type.Count; i++)
                {
                    comboType.Items.Add(type[i]);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private async void ButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string request = " ";
                if (comboType.SelectedIndex == -1 & comboMan.SelectedIndex == -1)
                {
                    throw new ArgumentException("Поле не може бути пусте");
                }

                if (comboType.SelectedIndex != -1 & comboMan.SelectedIndex != -1)
                {
                    throw new ArgumentException("Використовуйте лише один тип пошуку");
                }

                if (comboType.SelectedIndex != -1)
                {
                    request = tcpService.SerializeSearchByType(comboType.SelectedItem.ToString());
                }
                if (comboMan.SelectedIndex != -1)
                {
                    request = tcpService.SerializeSearchByProd(comboMan.SelectedItem.ToString());

                }

                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                List<MedicinesInfo> medic = tcpService.DeserializeSearch(response);
                dgg.ItemsSource = medic;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void ButtonCart_Click(object sender, RoutedEventArgs e)
        {

            if (sum.Text == "")

            {
                MessageBox.Show("Не вибрано жодного товару");
                return;
            }

            CheckOutForm checkOutForm = new CheckOutForm(sForm);
            checkOutForm.Cos.Text = this.sum.Text;

            checkOutForm.Show();
            this.Close();

        }

        private void dgg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid Dg = (DataGrid)sender;
            if (Dg.SelectedItem != null)
            {
                MedicinesInfo md = Dg.SelectedItem as MedicinesInfo;
                sum.Text = md.Price.ToString();
            }
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            sForm.Show();
            this.Close();
        }
    }
}
