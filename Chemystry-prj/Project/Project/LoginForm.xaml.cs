﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using Server;

namespace WpfApp1
{
    public static class SingletoneObj
    {
        public static Users User { get; set; }
        public static int Port { get; set; }
        public static string IP { get; set; }
        public static TcpClient Client { get; set; }
        public static NetworkStream Stream { get; set; }
    }
    public partial class LoginForm : Window
    {
        public readonly TcpService tcpService;
        public static string ip;
        public static int port;
        private bool active = false;
        public LoginForm()
        {
            InitializeComponent();
            this.tcpService = new TcpService();
            SingletoneObj.Port = 123;
            SingletoneObj.IP = "192.168.1.4";
            SingletoneObj.Client = new TcpClient(SingletoneObj.IP, SingletoneObj.Port);
            SingletoneObj.Stream = SingletoneObj.Client.GetStream();
        }
        

        
        private void ButtonSignUp_Click(object sender, RoutedEventArgs e)
        {
            RegistrationForm registrationForm = new RegistrationForm(this);
            registrationForm.Show();
            this.Hide();

        }
        private void ButtonSignIn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string login = loginField.Text;
                string password = passField.Password;

                inputNotNull(login);
                inputNotNull(password);

                string request = tcpService.SerializeAuthorizeRequest(login, password);
                byte[] data = tcpService.CodeStream(request);
                SingletoneObj.Stream.Write(data, 0, data.Length);
                string response = tcpService.DecodeStream(SingletoneObj.Stream);
                SingletoneObj.User = tcpService.DeserializeAuthorizeResponse(response);
                if (SingletoneObj.User.Login == null || SingletoneObj.User.Password == null || !SingletoneObj.User.Login.Equals(login) || !SingletoneObj.User.Password.Equals(password))
                    throw new ArgumentException("Логін або пароль не вірні");
                SearchForm searchForm = new SearchForm(this);
                searchForm.Show();
                this.Hide();

                loginField.Clear();
                passField.Clear();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public async void Back(bool mode)
        {
            if (mode == true && active == false)
            {
                string request = "LogOut";
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                Application.Current.Shutdown();
            }
            else if (mode == true && active == true)
            {
                this.Show();
                active = false;
            }
            else if (mode == false)
            {
                active = true;
            }
        }

        protected async override void OnClosed(EventArgs e)
        {
            this.Close();
            string request = "LogOut";
            byte[] data = await tcpService.CodeStreamAsync(request);
            await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
        }

        public static void inputNotNull(string st)
        {
            if (st.Length <= 0)
            {
                throw new ArgumentException("Поля не можуть бути пусті");
            }
        }


    }

}
