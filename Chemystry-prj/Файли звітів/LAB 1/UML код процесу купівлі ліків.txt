@startuml start
:Надіслати запит на купівлю ліків>
:Здійснити оплату>
if (Чи прийшла відповідь від серверу протягом 3с?) then (Так) :Аналіз відповіді від сервера;
:Отримати повідомлення про успішну оплату<
else (Ні)
:Отримання повідомлення про збій;
endif
stop @enduml
